
# Billennium interview project
> A project that tests the candidates' knowledge.

## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Tasks](#tasks)


## General Information
- The application is a simple Spring Boot 2 web application.
- Project uses MVC design pattern. Contains REST controllers, services and repositories.
- The user may find customer with invoices in declared year or get invoice by document number


## Technologies Used
- Java 17
- Spring Boot 2.6.3
- Flyway 8.0.5
- H2 Database 1.4.200
- Gradle 7.3.3


## Tasks
List of tasks here:
1) Write an endpoint for the customer where the result of the request will be customer data with invoices from a given year.
2) Write a service and a method of access to the database, which will return invoice with the invoice items for the given document number.
3) Write a method that calculates the invoice total gross for a given collection of invoice items.
4) Problem n + 1. There is an endpoint that returns all invoices with their items. This query is not optimized and there is an N + 1 problem. It should be solved.
5) Write tests to task no. 2