package com.interview.invoice.quest.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String documentNumber;
    private LocalDateTime saleDate;
    private LocalDateTime issueDate;

    @OneToMany
    @JoinColumn(name = "invoice_id")
    private Set<InvoiceItem> invoiceItems;
}
