package com.interview.invoice.quest.mapper;

import com.interview.invoice.quest.dto.InvoiceDto;
import com.interview.invoice.quest.model.Invoice;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface InvoiceMapper {

	@Mapping(target = "documentNumber", source = "documentNumber")
	@Mapping(target = "saleDate", source = "saleDate")
	@Mapping(target = "issueDate", source = "issueDate")
	@Mapping(target = "totalGross", expression = "java(com.interview.invoice.quest.utils.InvoiceCalculationsUtils.calculateTotalGross(invoice.getInvoiceItems()))")
	InvoiceDto convertInvoiceToInvoiceDto(Invoice invoice);

	@Mapping(target = "documentNumber", source = "documentNumber")
	@Mapping(target = "saleDate", source = "saleDate")
	@Mapping(target = "issueDate", source = "issueDate")
	@Mapping(target = "invoiceItemsDto", source = "invoiceItems")
	InvoiceDto convertInvoiceToInvoiceDtoWithInvoiceItems(Invoice invoice);
}
