package com.interview.invoice.quest.mapper;

import com.interview.invoice.quest.dto.CustomerDto;
import com.interview.invoice.quest.model.Customer;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface CustomerMapper {

	@Mapping(target = "name", source = "name")
	@Mapping(target = "taxNumber", source = "taxNumber")
	@Mapping(target = "city", source = "city")
	CustomerDto convertCustomerToCustomerDto(Customer customer);

	@Mapping(target = "name", source = "name")
	@Mapping(target = "taxNumber", source = "taxNumber")
	@Mapping(target = "city", source = "city")
	@Mapping(target = "invoiceDtos", source = "invoices")
	CustomerDto convertCustomerToCustomerWithInvoicesDto(Customer customer);
}
