package com.interview.invoice.quest.repository;

import com.interview.invoice.quest.model.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository for invoice operations.
 */
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {
	//TODO: Task no. 2
}
