package com.interview.invoice.quest.repository;

import com.interview.invoice.quest.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

	Optional<Customer> findByTaxNumber(String taxNumber);

	@Query("SELECT c FROM Customer c " +
			"LEFT JOIN FETCH c.invoices i " +
			"WHERE c.taxNumber = :taxNumber")
	Optional<Customer> findCustomerByTaxNumber(@Param("taxNumber") String taxNumber);

	@Query("SELECT c FROM Customer c " +
			"LEFT JOIN FETCH c.invoices i " +
			"WHERE c.taxNumber = :taxNumber " +
			"AND YEAR(i.issueDate) = :declaredYear")
	Optional<Customer> findCustomerByTaxNumberInYear(@Param("taxNumber") String taxNumber,
													 @Param("declaredYear") int year);

	void deleteByTaxNumber(String taxNumber);
}
