package com.interview.invoice.quest.controller;

import com.interview.invoice.quest.dto.CustomerDto;
import com.interview.invoice.quest.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * REST controller for customer operations.
 */
@RestController
@RequestMapping(value = "/customers")
@RequiredArgsConstructor
public class CustomerController {

	private final CustomerService customerService;


	//TODO: Task no. 1

	/**
	 * Get all customers
	 *
	 * @return list of customers
	 */
	@GetMapping
	public List<CustomerDto> getAllCustomers() {
		return customerService.getAllCustomers();
	}
}
