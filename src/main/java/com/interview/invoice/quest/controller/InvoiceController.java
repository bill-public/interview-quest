package com.interview.invoice.quest.controller;

import com.interview.invoice.quest.dto.InvoiceDto;
import com.interview.invoice.quest.service.InvoiceService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * REST controller for invoice operations.
 */
@RestController
@RequestMapping(value = "/invoices")
@RequiredArgsConstructor
public class InvoiceController {

	private final InvoiceService invoiceService;

	/**
	 * Get invoice by document number
	 *
	 * @param documentNumber document number
	 * @return invoiceDto
	 */
	@GetMapping(path = "/{documentNumber}")
	public InvoiceDto getInvoiceByDocumentNumber(@PathVariable("documentNumber") String documentNumber) {
		return invoiceService.getInvoiceByDocumentNumber(documentNumber);
	}

	/**
	 * Get all invoices with invoice items
	 *
	 * @return list of invoices
	 */
	@GetMapping
	public List<InvoiceDto> getAllInvoices() {
		return invoiceService.getAllInvoices();
	}
}
