package com.interview.invoice.quest.service;

import com.interview.invoice.quest.dto.CustomerDto;
import com.interview.invoice.quest.mapper.CustomerMapper;
import com.interview.invoice.quest.model.Customer;
import com.interview.invoice.quest.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service for customer operations.
 */
@Service
@RequiredArgsConstructor
public class CustomerService {

	private final CustomerRepository customerRepository;
	private final CustomerMapper customerMapper;

	/**
	 * Find customer with invoice
	 *
	 * @param taxNumber tax number
	 * @return customer with invoices
	 */
	public CustomerDto getCustomerByTaxNumber(String taxNumber) {
		Optional<Customer> customerOptional = customerRepository.findCustomerByTaxNumber(taxNumber);
		return customerOptional
				.map(customerMapper::convertCustomerToCustomerWithInvoicesDto)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No customer found"));
	}

	/**
	 * Find all customers
	 *
	 * @return list of customers
	 */
	public List<CustomerDto> getAllCustomers() {
		List<Customer> customerList = customerRepository.findAll();
		return customerList.stream()
				.map(customerMapper::convertCustomerToCustomerDto)
				.collect(Collectors.toList());
	}

	/**
	 * Find customer with invoices from declared year
	 *
	 * @param taxNumber tax number
	 * @param year      year of invoice issue date
	 * @return customer with invoices
	 */
	public CustomerDto getCustomerByTaxNumberInYear(String taxNumber, int year) {
		Optional<Customer> customerOptional = customerRepository.findCustomerByTaxNumberInYear(taxNumber, year);
		return customerOptional
				.map(customerMapper::convertCustomerToCustomerWithInvoicesDto)
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "No customer found"));
	}

	/**
	 * Add customer
	 *
	 * @param customerDto customer to create
	 * @return created customer
	 */
	public CustomerDto addCustomer(CustomerDto customerDto) {
		Optional<Customer> customerOptional = customerRepository.findByTaxNumber(customerDto.getTaxNumber());
		if (customerOptional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("Customer with tax number %s is already defined", customerDto.getTaxNumber()));
		}
		Customer customer = new Customer();
		customer.setTaxNumber(customerDto.getTaxNumber());
		customer.setCity(customerDto.getCity());
		customer.setName(customerDto.getName());
		return customerMapper.convertCustomerToCustomerDto(customerRepository.save(customer));
	}

	/**
	 * Edit customer
	 *
	 * @param customerDto customer to edit
	 * @return edited customer
	 */
	public CustomerDto editCustomer(CustomerDto customerDto) {
		Optional<Customer> customerOptional = customerRepository.findByTaxNumber(customerDto.getTaxNumber());
		if (customerOptional.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No customer found");
		}
		Customer customer = customerOptional.get();
		customer.setCity(customerDto.getCity());
		customer.setName(customerDto.getName());
		return customerMapper.convertCustomerToCustomerDto(customerRepository.save(customer));
	}

	/**
	 * Delete customer
	 *
	 * @param customerDto customer to delete
	 */
	public void deleteCustomer(CustomerDto customerDto) {
		Optional<Customer> customerOptional = customerRepository.findByTaxNumber(customerDto.getTaxNumber());
		if (customerOptional.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No customer found");
		}
		customerRepository.deleteByTaxNumber(customerDto.getTaxNumber());
	}
}
