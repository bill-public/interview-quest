package com.interview.invoice.quest.service;

import com.interview.invoice.quest.dto.InvoiceDto;
import com.interview.invoice.quest.mapper.InvoiceMapper;
import com.interview.invoice.quest.model.Invoice;
import com.interview.invoice.quest.repository.InvoiceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Service for invoice operations.
 */
//TODO: Task no. 2
@Service
@RequiredArgsConstructor
public class InvoiceService {

	private final InvoiceRepository invoiceRepository;
	private final InvoiceMapper invoiceMapper;

	/**
	 * Get invoice with total gross by document number
	 *
	 * @param documentNumber document number
	 * @return invoice with total gross
	 */
	public InvoiceDto getInvoiceByDocumentNumber(String documentNumber) {
		return null;
	}

	/**
	 * Get all invoices with invoice items
	 *
	 * @return list of invoices
	 */
	//TODO: Task no. 4
	public List<InvoiceDto> getAllInvoices() {
		List<Invoice> invoiceList = invoiceRepository.findAll();
		return invoiceList.stream()
				.map(invoiceMapper::convertInvoiceToInvoiceDtoWithInvoiceItems)
				.collect(Collectors.toList());
	}
}
