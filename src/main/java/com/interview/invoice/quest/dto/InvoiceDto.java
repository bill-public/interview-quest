package com.interview.invoice.quest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.Set;

@Getter
@Setter
@Builder
public class InvoiceDto {
	private String documentNumber;
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime saleDate;
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime issueDate;
	private double totalGross;
	private Set<InvoiceItemDto> invoiceItemsDto;
}
