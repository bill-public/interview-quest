package com.interview.invoice.quest.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@Builder
public class CustomerDto {
	private String name;
	private String taxNumber;
	private String city;
	private Set<InvoiceDto> invoiceDtos;
}
