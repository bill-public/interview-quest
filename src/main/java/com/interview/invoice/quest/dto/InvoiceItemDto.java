package com.interview.invoice.quest.dto;

import com.interview.invoice.quest.model.InvoiceItemName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class InvoiceItemDto {
	private InvoiceItemName invoiceItemName;
	private double quantity;
	private double netPrice;
	private int vatRate;
}
