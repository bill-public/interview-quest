package com.interview.invoice.quest.utils;

import com.interview.invoice.quest.model.InvoiceItem;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Collection;

/**
 * Utils class for operations on invoice
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class InvoiceCalculationsUtils {

	/**
	 * Calculate total gross
	 * @param invoiceItems items on invoice
	 * @return total gross
	 */
	//TODO: Task no. 3
	public static double calculateTotalGross(Collection<InvoiceItem> invoiceItems) {
		return 0d;
	}
}
