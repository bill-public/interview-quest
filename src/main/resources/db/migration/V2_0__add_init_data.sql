INSERT INTO customer (id, name, tax_number, city) VALUES (1, 'Marek Kowalski', '8123871231', 'Łowicz');
INSERT INTO customer (id, name, tax_number, city) VALUES (2, 'Michał Bednarek', '9291838342', 'Radom');
INSERT INTO customer (id, name, tax_number, city) VALUES (3, 'Mariusz Nowak', '21346345', 'Warszawa');
INSERT INTO customer (id, name, tax_number, city) VALUES (4, 'Tomasz Kowalczyk', '67343587', 'Warszawa');

INSERT INTO invoice (id, document_number, sale_date, issue_date, customer_id) VALUES (1, 'FV_01', '2021-12-12 13:12:21', '2022-12-07 15:22:21', 1);
INSERT INTO invoice (id, document_number, sale_date, issue_date, customer_id) VALUES (2, 'FV_02', '2022-03-01 22:22:11', '2022-02-22 7:49:21', 1);
INSERT INTO invoice (id, document_number, sale_date, issue_date, customer_id) VALUES (3, 'FV_03', '2020-03-14 12:41:17', '2022-02-11 2:31:52', 2);
INSERT INTO invoice (id, document_number, sale_date, issue_date, customer_id) VALUES (4, 'FV_04', '2021-02-04 17:49:33', '2020-01-26 11:23:56', 3);
INSERT INTO invoice (id, document_number, sale_date, issue_date, customer_id) VALUES (5, 'FV_05', '2019-06-23 12:33:41', '2019-04-23 12:31:23', 1);
INSERT INTO invoice (id, document_number, sale_date, issue_date, customer_id) VALUES (6, 'FV_06', '2021-12-16 17:47:56', '2021-11-24 2:12:56', 2);
INSERT INTO invoice (id, document_number, sale_date, issue_date, customer_id) VALUES (7, 'FV_07', '2021-11-04 12:14:31', '2021-06-24 2:04:33', 3);
INSERT INTO invoice (id, document_number, sale_date, issue_date, customer_id) VALUES (8, 'FV_08', '2020-09-17 06:19:54', '2020-02-11 2:51:26', 3);
INSERT INTO invoice (id, document_number, sale_date, issue_date, customer_id) VALUES (9, 'FV_09', '2021-06-04 12:48:31', '2021-01-23 2:27:18', 4);

INSERT INTO invoice_item (id, name, quantity, net_price, vat_rate, invoice_id) VALUES (1, 'PAPERLESS_OFFICE', 2, 122300.12, 23, 1);
INSERT INTO invoice_item (id, name, quantity, net_price, vat_rate, invoice_id) VALUES (2, 'INPERLY', 1, 13832.12, 23, 1);
INSERT INTO invoice_item (id, name, quantity, net_price, vat_rate, invoice_id) VALUES (3, 'PAPERLESS_OFFICE', 1, 61150.06, 23, 2);
INSERT INTO invoice_item (id, name, quantity, net_price, vat_rate, invoice_id) VALUES (4, 'DEV_MD', 2.4, 3221.53, 13, 2);
INSERT INTO invoice_item (id, name, quantity, net_price, vat_rate, invoice_id) VALUES (5, 'DEV_MD', 5.8, 15000.53, 13, 3);
INSERT INTO invoice_item (id, name, quantity, net_price, vat_rate, invoice_id) VALUES (6, 'PAPERLESS_OFFICE', 1, 61150.06, 23, 4);
INSERT INTO invoice_item (id, name, quantity, net_price, vat_rate, invoice_id) VALUES (7, 'DEV_MD', 47.5, 253472.52, 13, 5);
INSERT INTO invoice_item (id, name, quantity, net_price, vat_rate, invoice_id) VALUES (8, 'PAPERLESS_OFFICE', 11, 48121.23, 23, 6);
INSERT INTO invoice_item (id, name, quantity, net_price, vat_rate, invoice_id) VALUES (9, 'DEV_MD', 1, 1231.72, 13, 7);
INSERT INTO invoice_item (id, name, quantity, net_price, vat_rate, invoice_id) VALUES (10, 'INPERLY', 7, 91025.26, 23, 8);
INSERT INTO invoice_item (id, name, quantity, net_price, vat_rate, invoice_id) VALUES (11, 'DEV_MD', 11.4, 34123.84, 13, 9);
INSERT INTO invoice_item (id, name, quantity, net_price, vat_rate, invoice_id) VALUES (12, 'INPERLY', 3, 35923.74, 23, 9);