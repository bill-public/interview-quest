CREATE TABLE customer (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    tax_number VARCHAR(100) NOT NULL UNIQUE,
    city VARCHAR(100)
);

CREATE TABLE invoice (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    document_number VARCHAR(50) NOT NULL UNIQUE,
    sale_date TIMESTAMP NOT NULL,
    issue_date TIMESTAMP NOT NULL,
    customer_id BIGINT NOT NULL
);

CREATE TABLE invoice_item (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    quantity DECIMAL(15, 1) NOT NULL,
    net_price DECIMAL(15, 2) NOT NULL,
    vat_rate INT NOT NULL,
    invoice_id BIGINT NOT NULL
);

ALTER TABLE invoice
    ADD CONSTRAINT invoice_customer_id
    FOREIGN KEY (customer_id) REFERENCES customer(id);

ALTER TABLE invoice_item
    ADD CONSTRAINT invoice_item_invoice_id
    FOREIGN KEY (invoice_id) REFERENCES invoice(id);