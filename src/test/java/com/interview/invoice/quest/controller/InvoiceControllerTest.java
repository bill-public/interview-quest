package com.interview.invoice.quest.controller;

import com.interview.invoice.quest.dto.InvoiceDto;
import com.interview.invoice.quest.helper.InvoiceHelper;
import com.interview.invoice.quest.service.InvoiceService;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

class InvoiceControllerTest {

	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
	private static final String DOCUMENT_NUMBER = "FV123_123";
	private static final LocalDateTime ISSUE_DATE = LocalDateTime.of(2022, 3, 12, 12, 14, 52);
	private static final LocalDateTime SALE_DATE = LocalDateTime.of(2022, 2, 12, 10, 12, 12);
	private static final double TOTAL_GROSS = 4123.23d;

	@InjectMocks
	private InvoiceController invoiceController;

	@Mock
	private InvoiceService invoiceService;

	private MockMvc mockMvc;

	@BeforeEach
	public void initBeforeMethod() {
		MockitoAnnotations.openMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(invoiceController)
				.build();
	}

	@Test
	public void getInvoiceByDocumentNumberTest() throws Exception {
		//given
		InvoiceDto invoiceDto = InvoiceHelper.getInvoiceDto(DOCUMENT_NUMBER, ISSUE_DATE, SALE_DATE, TOTAL_GROSS);
		Mockito.when(invoiceService.getInvoiceByDocumentNumber(Mockito.eq(DOCUMENT_NUMBER)))
				.thenReturn(invoiceDto);

		//when
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/invoices/" + DOCUMENT_NUMBER)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andReturn();

		//then
		DocumentContext result = JsonPath.parse(mvcResult.getResponse().getContentAsString());
		Assertions.assertEquals(DOCUMENT_NUMBER, result.read("documentNumber"));
		Assertions.assertEquals(ISSUE_DATE.format(FORMATTER), result.read("issueDate"));
		Assertions.assertEquals(SALE_DATE.format(FORMATTER), result.read("saleDate"));
		Assertions.assertEquals(TOTAL_GROSS, result.read("totalGross"));
	}

	@Test
	public void getAllInvoicesTest() throws Exception {
		//given
		InvoiceDto invoiceDto = InvoiceHelper.getInvoiceDto(DOCUMENT_NUMBER, ISSUE_DATE, SALE_DATE, TOTAL_GROSS);
		Mockito.when(invoiceService.getAllInvoices()).thenReturn(List.of(invoiceDto));

		//when
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/invoices")
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andReturn();

		//then
		DocumentContext result = JsonPath.parse(mvcResult.getResponse().getContentAsString());
		Assertions.assertEquals(DOCUMENT_NUMBER, result.read("[0].documentNumber"));
		Assertions.assertEquals(ISSUE_DATE.format(FORMATTER), result.read("[0].issueDate"));
		Assertions.assertEquals(SALE_DATE.format(FORMATTER), result.read("[0].saleDate"));
		Assertions.assertEquals(TOTAL_GROSS, result.read("[0].totalGross"));
	}
}