package com.interview.invoice.quest.controller;

import com.interview.invoice.quest.dto.CustomerDto;
import com.interview.invoice.quest.helper.CustomerHelper;
import com.interview.invoice.quest.service.CustomerService;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

class CustomerControllerTest {

	private static final String CUSTOMER_NAME = "Mariusz Nowak";
	private static final String TAX_NUMBER = "21346345";
	private static final String CITY = "Warszawa";
	private static final int CURRENT_YEAR = LocalDateTime.now().getYear();

	@InjectMocks
	private CustomerController customerController;

	@Mock
	private CustomerService customerService;

	private MockMvc mockMvc;

	@BeforeEach
	public void initBeforeMethod() {
		MockitoAnnotations.openMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(customerController)
				.build();
	}

	@Test
	public void getCustomerByTaxNumberInYearTest() throws Exception {
		//given
		CustomerDto customerDto = CustomerHelper.getCustomerDto(CUSTOMER_NAME, TAX_NUMBER, CITY, Collections.emptySet());
		Mockito.when(customerService.getCustomerByTaxNumberInYear(Mockito.eq(TAX_NUMBER), Mockito.eq(CURRENT_YEAR)))
				.thenReturn(customerDto);

		//when
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/customers/" + TAX_NUMBER + "/" + CURRENT_YEAR)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andReturn();

		//then
		DocumentContext result = JsonPath.parse(mvcResult.getResponse().getContentAsString());
		Assertions.assertEquals(CUSTOMER_NAME, result.read("name"));
		Assertions.assertEquals(TAX_NUMBER, result.read("taxNumber"));
		Assertions.assertEquals(CITY, result.read("city"));
	}

	@Test
	public void getAllCustomersTest() throws Exception {
		//given
		CustomerDto customerDto = CustomerHelper.getCustomerDto(CUSTOMER_NAME, TAX_NUMBER, CITY, Collections.emptySet());
		Mockito.when(customerService.getAllCustomers()).thenReturn(List.of(customerDto));

		//when
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/customers")
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andReturn();

		//then
		DocumentContext result = JsonPath.parse(mvcResult.getResponse().getContentAsString());
		Assertions.assertEquals(CUSTOMER_NAME, result.read("[0].name"));
		Assertions.assertEquals(TAX_NUMBER, result.read("[0].taxNumber"));
		Assertions.assertEquals(CITY, result.read("[0].city"));
	}
}