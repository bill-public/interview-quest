package com.interview.invoice.quest.helper;

import com.interview.invoice.quest.dto.CustomerDto;
import com.interview.invoice.quest.dto.InvoiceDto;

import java.util.Set;

public class CustomerHelper {

	public static CustomerDto getCustomerDto(String name, String taxNumber, String city, Set<InvoiceDto> invoiceDtos) {
		return CustomerDto.builder()
				.name(name)
				.taxNumber(taxNumber)
				.city(city)
				.invoiceDtos(invoiceDtos)
				.build();
	}
}
