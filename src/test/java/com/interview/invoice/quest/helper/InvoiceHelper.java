package com.interview.invoice.quest.helper;

import com.interview.invoice.quest.dto.InvoiceDto;
import com.interview.invoice.quest.model.Invoice;
import com.interview.invoice.quest.model.InvoiceItem;
import com.interview.invoice.quest.model.InvoiceItemName;

import java.time.LocalDateTime;
import java.util.Set;

public class InvoiceHelper {

	public static Invoice getInvoice(String documentNumber, LocalDateTime issueDate, LocalDateTime saleDate, Set<InvoiceItem> invoiceItems) {
		Invoice invoice = new Invoice();
		invoice.setDocumentNumber(documentNumber);
		invoice.setIssueDate(issueDate);
		invoice.setSaleDate(saleDate);
		invoice.setInvoiceItems(invoiceItems);

		return invoice;
	}

	public static InvoiceItem getInvoiceItem(InvoiceItemName name, double quantity, double netPrice, int vatRate) {
		InvoiceItem invoiceItem = new InvoiceItem();
		invoiceItem.setInvoiceItemName(name);
		invoiceItem.setQuantity(quantity);
		invoiceItem.setNetPrice(netPrice);
		invoiceItem.setVatRate(vatRate);

		return invoiceItem;
	}

	public static InvoiceDto getInvoiceDto(String documentNumber, LocalDateTime issueDate, LocalDateTime saleDate, double totalGross) {
		return InvoiceDto.builder()
				.documentNumber(documentNumber)
				.issueDate(issueDate)
				.saleDate(saleDate)
				.totalGross(totalGross)
				.build();
	}
}
