package com.interview.invoice.quest.utils;

import com.interview.invoice.quest.helper.InvoiceHelper;
import com.interview.invoice.quest.model.InvoiceItem;
import com.interview.invoice.quest.model.InvoiceItemName;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Set;


class InvoiceCalculationsUtilsTest {

	@Test
	public void calculateTotalGrossOfInvoiceWithTwoItemsTest() {
		//given
		InvoiceItem firstItem = InvoiceHelper.getInvoiceItem(InvoiceItemName.PAPERLESS_OFFICE, 2d, 100d, 23);
		InvoiceItem secondItem = InvoiceHelper.getInvoiceItem(InvoiceItemName.DEV_MD, 2.7d, 100.4d, 10);

		//when
		double output = InvoiceCalculationsUtils.calculateTotalGross(Set.of(firstItem, secondItem));

		//then
		Assertions.assertEquals(544.19d, output);
	}

	@Test
	public void calculateTotalGrossOfInvoiceWithOneItemTest() {
		//given
		InvoiceItem item = InvoiceHelper.getInvoiceItem(InvoiceItemName.DEV_MD, 7.7d, 101.4d, 10);

		//when
		double output = InvoiceCalculationsUtils.calculateTotalGross(Set.of(item));

		//then
		Assertions.assertEquals(858.86d, output);
	}

	@Test
	public void calculateTotalGrossOfInvoiceWithOneItemZeroVatTest() {
		//given
		InvoiceItem item = InvoiceHelper.getInvoiceItem(InvoiceItemName.DEV_MD, 7.7d, 101.4d, 0);

		//when
		double output = InvoiceCalculationsUtils.calculateTotalGross(Set.of(item));

		//then
		Assertions.assertEquals(780.78d, output);
	}

	@Test
	public void calculateTotalGrossOfInvoiceWithOneItemZeroNetPriceTest() {
		//given
		InvoiceItem item = InvoiceHelper.getInvoiceItem(InvoiceItemName.DEV_MD, 9.7d, 0d, 12);

		//when
		double output = InvoiceCalculationsUtils.calculateTotalGross(Set.of(item));

		//then
		Assertions.assertEquals(0, output);
	}

	@Test
	public void calculateTotalGrossOfInvoiceWithOneItemZeroQuantityTest() {
		//given
		InvoiceItem item = InvoiceHelper.getInvoiceItem(InvoiceItemName.DEV_MD, 0d, 123.12d, 12);

		//when
		double output = InvoiceCalculationsUtils.calculateTotalGross(Set.of(item));

		//then
		Assertions.assertEquals(0, output);
	}

	@Test
	public void calculateTotalGrossOfInvoiceWithoutItemsTest() {
		//given

		//when
		double output = InvoiceCalculationsUtils.calculateTotalGross(Collections.emptySet());

		//then
		Assertions.assertEquals(0, output);
	}
}