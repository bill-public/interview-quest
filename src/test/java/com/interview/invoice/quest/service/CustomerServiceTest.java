package com.interview.invoice.quest.service;

import com.interview.invoice.quest.dto.CustomerDto;
import com.interview.invoice.quest.helper.CustomerHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;


@SpringBootTest
class CustomerServiceTest {

	private static final String TAX_NUMBER_WITH_TWO_INVOICES = "8123871231";
	private static final String TAX_NUMBER_WITH_ONE_INVOICE = "9291838342";
	private static final String TAX_NUMBER_WITHOUT_INVOICE = "21346345";
	private static final String TAX_NUMBER_INVOICE_TO_DELETE = "67343587";
	private static final String NEW_TAX_NUMBER = "5635434654";
	private static final String CITY = "Szczecin";
	private static final String FAKE_TAX_NUMBER = "123";
	private static final String NAME = "Tomasz Kowalski";
	private static final int ISSUE_DATE_YEAR_WITH_INVOICES = 2022;
	private static final int ISSUE_DATE_YEAR_WITHOUT_INVOICES = 2021;

	@Autowired
	private CustomerService customerService;

	@Test
	void getCustomerByTaxNumberTest() {
		//when
		CustomerDto output = customerService.getCustomerByTaxNumber(TAX_NUMBER_WITH_TWO_INVOICES);

		//then
		Assertions.assertEquals(TAX_NUMBER_WITH_TWO_INVOICES, output.getTaxNumber());
	}

	@Test
	void getCustomerByTaxNumberNoResultTest() {
		//when
		try {
			customerService.getCustomerByTaxNumber(FAKE_TAX_NUMBER);
			Assertions.fail("No exception is thrown");
		} catch (ResponseStatusException ex) {
			//then
			Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
			Assertions.assertEquals("No customer found", ex.getReason());
		}
	}

	@Test
	void getAllCustomersTest() {
		//when
		List<CustomerDto> output = customerService.getAllCustomers();

		//then
		Assertions.assertTrue(output.size() > 1);
	}

	@Test
	void getCustomerWithTwoInvoicesTest() {
		//when
		CustomerDto output = customerService.getCustomerByTaxNumberInYear(TAX_NUMBER_WITH_TWO_INVOICES, ISSUE_DATE_YEAR_WITH_INVOICES);

		//then
		Assertions.assertEquals(TAX_NUMBER_WITH_TWO_INVOICES, output.getTaxNumber());
		Assertions.assertEquals(2, output.getInvoiceDtos().size());
	}

	@Test
	void getCustomerWithOneInvoiceTest() {
		//when
		CustomerDto output = customerService.getCustomerByTaxNumberInYear(TAX_NUMBER_WITH_ONE_INVOICE, ISSUE_DATE_YEAR_WITH_INVOICES);

		//then
		Assertions.assertEquals(TAX_NUMBER_WITH_ONE_INVOICE, output.getTaxNumber());
		Assertions.assertEquals(1, output.getInvoiceDtos().size());
	}

	@Test
	void getCustomerByTaxNumberInYearNoResultTest() {
		//when
		try {
			customerService.getCustomerByTaxNumberInYear(TAX_NUMBER_WITHOUT_INVOICE, ISSUE_DATE_YEAR_WITH_INVOICES);
			Assertions.fail("No exception is thrown");
		} catch (ResponseStatusException ex) {
			//then
			Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
			Assertions.assertEquals("No customer found", ex.getReason());
		}
	}

	@Test
	void getCustomerWithoutInvoiceInExactYearTest() {
		//when
		try {
			customerService.getCustomerByTaxNumberInYear(TAX_NUMBER_WITH_TWO_INVOICES, ISSUE_DATE_YEAR_WITHOUT_INVOICES);
			Assertions.fail("No exception is thrown");
		} catch (ResponseStatusException ex) {
			//then
			Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
			Assertions.assertEquals("No customer found", ex.getReason());
		}
	}

	@Test
	void addCustomerTest() {
		//given
		CustomerDto input = CustomerHelper.getCustomerDto(NAME, NEW_TAX_NUMBER, CITY, Collections.emptySet());

		//when
		CustomerDto output = customerService.addCustomer(input);

		//then
		Assertions.assertEquals(NAME, output.getName());
		Assertions.assertEquals(NEW_TAX_NUMBER, output.getTaxNumber());
		Assertions.assertEquals(CITY, output.getCity());
		Assertions.assertNull(output.getInvoiceDtos());
	}

	@Test
	void addAlreadyDefinedCustomerTest() {
		//given
		CustomerDto input = CustomerHelper.getCustomerDto(NAME, TAX_NUMBER_WITH_TWO_INVOICES, CITY, Collections.emptySet());

		//when
		try {
			customerService.addCustomer(input);
			Assertions.fail("No exception is thrown");
		} catch (ResponseStatusException ex) {
			//then
			Assertions.assertEquals(HttpStatus.BAD_REQUEST, ex.getStatus());
			Assertions.assertEquals(String.format("Customer with tax number %s is already defined", TAX_NUMBER_WITH_TWO_INVOICES), ex.getReason());
		}
	}

	@Test
	void editCustomerTest() {
		//given
		CustomerDto input = CustomerHelper.getCustomerDto(NAME, TAX_NUMBER_WITHOUT_INVOICE, CITY, Collections.emptySet());

		//when
		CustomerDto output = customerService.editCustomer(input);

		//then
		Assertions.assertEquals(NAME, output.getName());
		Assertions.assertEquals(TAX_NUMBER_WITHOUT_INVOICE, output.getTaxNumber());
		Assertions.assertEquals(CITY, output.getCity());
		Assertions.assertNull(output.getInvoiceDtos());
	}

	@Test
	void tryToEditNotExistingCustomerTest() {
		//given
		CustomerDto input = CustomerHelper.getCustomerDto(NAME, FAKE_TAX_NUMBER, CITY, Collections.emptySet());

		//when
		try {
			customerService.editCustomer(input);
			Assertions.fail("No exception is thrown");
		} catch (ResponseStatusException ex) {
			//then
			Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
			Assertions.assertEquals("No customer found", ex.getReason());
		}
	}

	@Test
	@Transactional
	void deleteCustomerTest() {
		//given
		CustomerDto input = CustomerHelper.getCustomerDto(NAME, TAX_NUMBER_INVOICE_TO_DELETE, CITY, Collections.emptySet());

		//when
		customerService.deleteCustomer(input);

		//then not thrown exception
	}

	@Test
	void tryToDeleteNotExistingCustomerTest() {
		//given
		CustomerDto input = CustomerHelper.getCustomerDto(NAME, FAKE_TAX_NUMBER, CITY, Collections.emptySet());

		//when
		try {
			customerService.deleteCustomer(input);
			Assertions.fail("No exception is thrown");
		} catch (ResponseStatusException ex) {
			//then
			Assertions.assertEquals(HttpStatus.NOT_FOUND, ex.getStatus());
			Assertions.assertEquals("No customer found", ex.getReason());
		}
	}
}